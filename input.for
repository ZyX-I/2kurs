      SUBROUTINE INSTR(IN, MAXL, L)
        CHARACTER IN*(*)
        INTEGER L, MAXL
        INTEGER CI
        CHARACTER C
        L=0
  100     READ(*,'(A,$)') C
          CI=ICHAR(C)
          IF(33 .LE. CI .AND. CI .LE. 128)THEN
            L=L+1
            IN(L:L)=C
            IF(L .NE. MAXL)THEN
              GO TO 100
            END IF
          END IF
      END SUBROUTINE
      SUBROUTINE INDBL(X)
        DOUBLE PRECISION X
        CHARACTER XS*128, C, F*14
        INTEGER W, D, E, I, LI
        LOGICAL HSGN, HDIG, HDEC, HEXP, HESG, HEDG
        W=0
        D=0
        E=0
        HSGN=.FALSE.
        HDIG=.FALSE.
        HDEC=.FALSE.
        HEXP=.FALSE.
        HESG=.FALSE.
        HEDG=.FALSE.
  200     READ(*,'(A,$)')C
          IF(C .EQ. '+' .OR. C .EQ. '-')THEN
            IF(HSGN .AND. .NOT. HDIG)THEN
              STOP 'Duplicate sign'
            END IF
            IF(HDIG .AND. .NOT. HEXP)THEN
              STOP 'Sign inside number'
            END IF
            IF(HESG)THEN
              STOP 'Duplicate exponent sign'
            END IF
            IF(HEDG)THEN
              STOP 'No place for sign anymore'
            END IF
            IF(HSGN .OR. HDIG)THEN
              HESG=.TRUE.
            ELSE
              HSGN=.TRUE.
            END IF
            W=W+1
            XS(W:W)=C
            GO TO 200
          END IF
          IF(C .EQ. '.')THEN
            IF(HDEC)THEN
              STOP 'Duplicate decimal dot'
            END IF
            IF(.NOT. HDIG)THEN
              W=W+1
              XS(W:W)='0'
              HDIG=.TRUE.
            END IF
            W=W+1
            XS(W:W)=C
            HDEC=.TRUE.
            GO TO 200
          END IF
          IF(C .EQ. 'e')THEN
            IF(HEXP)THEN
              STOP 'Duplicate exponent'
            END IF
            IF(.NOT. HDIG)THEN
              STOP 'No mantiss'
            END IF
            IF(.NOT. HDEC)THEN
              W=W+1
              XS(W:W)='.'
              W=W+1
              XS(W:W)='0'
              HDEC=.TRUE.
            END IF
            W=W+1
            XS(W:W)='E'
            HEXP=.TRUE.
            GO TO 200
          END IF
          CI=ICHAR(C)
          IF(ICHAR('0') .LE. CI .AND. CI .LE. ICHAR('9'))THEN
            IF(HDIG)THEN
              IF(HEXP)THEN
                HEDG=.TRUE.
                E=E+1
              ELSE
                IF(HDEC)THEN
                  D=D+1
                END IF
              END IF
            ELSE
              HDIG=.TRUE.
            END IF
            W=W+1
            XS(W:W)=C
            GO TO 200
          END IF
        IF(.NOT. HDEC)THEN
          W=W+1
          XS(W:W)='.'
          W=W+1
          XS(W:W)='0'
          W=W+1
          XS(W:W)='E'
          W=W+1
          XS(W:W)='0'
          D=1
          E=1
        ELSE
          IF(.NOT. HEXP)THEN
            W=W+1
            XS(W:W)='E'
            W=W+1
            XS(W:W)='0'
            E=1
          END IF
        END IF
        WRITE(F,'(A,I3,A,I3,A,I3,A)')'(E',W,'.',D,'E',E,')'
        LI=INDEX(F, ')')
  210     I=INDEX(F, ' ')
          IF(I .NE. 0 .AND. I .LT. LI)THEN
            F(I:13)=F(I+1:14)
            F(14:14)=' '
            LI=LI-1
            GO TO 210
          END IF
        READ(XS,F)X
      END SUBROUTINE
      SUBROUTINE ININT(N)
        INTEGER N
        CHARACTER C
        INTEGER CI, SGN
        SGN=1
        N=0
  300     READ(*,'(A,$)')C
          IF(C .EQ. '-')THEN
            SGN=-1
            GO TO 300
          END IF
          CI=ICHAR(C)
          IF(ICHAR('0') .LE. CI .AND. CI .LE. ICHAR('9'))THEN
            N=N*10+CI-ICHAR('0')
            GO TO 300
          END IF
        N=SGN*N
      END SUBROUTINE
c vim: ts=2 sts=2 sw=2 tw=72 ft=fortran fenc=utf-8
