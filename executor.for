      SUBROUTINE EXECUTE(OUT, L, VARS, R)
        CHARACTER OUT*(*)
        INTEGER L
        DOUBLE PRECISION VARS(ICHAR('Z')-ICHAR('A')+1)
        DOUBLE PRECISION R
        DOUBLE PRECISION STACK(128), V
        INTEGER SP, OP, CI
        CHARACTER C
        INCLUDE "constants.for"
        SP=1
        OP=0
  100     OP=OP+1
          IF(OP .GT. L)THEN
            GO TO 200
          END IF
          C=OUT(OP:OP)
          IF(C .EQ. '+')THEN
            STACK(SP-1)=STACK(SP-1)+STACK(SP)
            SP=SP-1
            GO TO 100
          END IF
          IF(C .EQ. '-')THEN
            STACK(SP-1)=STACK(SP-1)-STACK(SP)
            SP=SP-1
            GO TO 100
          END IF
          IF(C .EQ. '*')THEN
            STACK(SP-1)=STACK(SP-1)*STACK(SP)
            SP=SP-1
            GO TO 100
          END IF
          IF(C .EQ. '/')THEN
            STACK(SP-1)=STACK(SP-1)/STACK(SP)
            SP=SP-1
            GO TO 100
          END IF
          IF(C .EQ. '^')THEN
            STACK(SP-1)=STACK(SP-1)**STACK(SP)
            SP=SP-1
            GO TO 100
          END IF
          IF(C .EQ. '!')THEN
            STACK(SP)=-STACK(SP)
            GO TO 100
          END IF
          IF(C .EQ. F_SIN)THEN
            STACK(SP)=SIN(STACK(SP))
            GO TO 100
          END IF
          IF(C .EQ. F_COS)THEN
            STACK(SP)=COS(STACK(SP))
            GO TO 100
          END IF
          IF(C .EQ. F_TG)THEN
            STACK(SP)=TAN(STACK(SP))
            GO TO 100
          END IF
          IF(C .EQ. F_ASIN)THEN
            STACK(SP)=ASIN(STACK(SP))
            GO TO 100
          END IF
          IF(C .EQ. F_ACOS)THEN
            STACK(SP)=ACOS(STACK(SP))
            GO TO 100
          END IF
          IF(C .EQ. F_ATG)THEN
            STACK(SP)=ATAN(STACK(SP))
            GO TO 100
          END IF
          IF(C .EQ. F_SH)THEN
            V=STACK(SP)
            STACK(SP)=(EXP(V)-EXP(-V))/2
            GO TO 100
          END IF
          IF(C .EQ. F_CH)THEN
            V=STACK(SP)
            STACK(SP)=(EXP(V)+EXP(-V))/2
            GO TO 100
          END IF
          IF(C .EQ. F_EXP)THEN
            STACK(SP)=EXP(STACK(SP))
            GO TO 100
          END IF
          IF(C .EQ. F_LN)THEN
            STACK(SP)=LOG(STACK(SP))
            GO TO 100
          END IF
          IF(C .EQ. C_PI)THEN
            STACK(SP+1)=3.1415926535897932384626433832795028841971D0
            SP=SP+1
            GO TO 100
          END IF
          CI=ICHAR(C)
          IF(      ICHAR('A') .LE. CI
     $       .AND. CI         .LE. ICHAR('Z'))THEN
            STACK(SP+1)=VARS(CI-ICHAR('A')+1)
            SP=SP+1
            GO TO 100
          END IF
          IF(      ICHAR('0') .LE. CI
     $       .AND. CI         .LE. ICHAR('9'))THEN
            STACK(SP+1)=DBLE(CI-ICHAR('0'))
            SP=SP+1
            GO TO 100
          END IF
  200   R=STACK(SP)
      END SUBROUTINE
c vim: ts=2 sts=2 sw=2 tw=72 ft=fortran fenc=utf-8
