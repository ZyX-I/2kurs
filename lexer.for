c     PROGRAM LEXER
c       CHARACTER INPUT*128, OUTPUT*128
c       INTEGER I
c       READ(*,'(A128)') INPUT
c       DO 25, I=1,128
c         OUTPUT(I:I)=' '
c 25    END DO
c       CALL LEX(INPUT, OUTPUT)
c       WRITE(*,'(A128)') OUTPUT
c     END PROGRAM
      SUBROUTINE LEX(IN, OUT, IL, OL)
        CHARACTER IN*(*), OUT*(*)
        CHARACTER C
        INTEGER IL, OL
        INTEGER I, CI, CAP
        PARAMETER (CAP = ICHAR('A')-ICHAR('a'))
        LOGICAL STRMATCH
        INCLUDE "constants.for"
        OL=1
        I=1
  50    CONTINUE
          IF(STRMATCH('sin', IN, I, IL))THEN
            CALL PUSH(OUT, OL, F_SIN)
            I=I+3
            GO TO 75
          END IF
          IF(STRMATCH('cos', IN, I, IL))THEN
            CALL PUSH(OUT, OL, F_COS)
            I=I+3
            GO TO 75
          END IF
          IF(STRMATCH('tg', IN, I, IL))THEN
            CALL PUSH(OUT, OL, F_TG)
            I=I+2
            GO TO 75
          END IF
          IF(STRMATCH('asin', IN, I, IL))THEN
            CALL PUSH(OUT, OL, F_ASIN)
            I=I+4
            GO TO 75
          END IF
          IF(STRMATCH('acos', IN, I, IL))THEN
            CALL PUSH(OUT, OL, F_ACOS)
            I=I+4
            GO TO 75
          END IF
          IF(STRMATCH('atg', IN, I, IL))THEN
            CALL PUSH(OUT, OL, F_ATG)
            I=I+3
            GO TO 75
          END IF
          IF(STRMATCH('exp', IN, I, IL))THEN
            CALL PUSH(OUT, OL, F_EXP)
            I=I+3
            GO TO 75
          END IF
          IF(STRMATCH('ln', IN, I, IL))THEN
            CALL PUSH(OUT, OL, F_LN)
            I=I+2
            GO TO 75
          END IF
          IF(STRMATCH('sh', IN, I, IL))THEN
            CALL PUSH(OUT, OL, F_SH)
            I=I+2
            GO TO 75
          END IF
          IF(STRMATCH('ch', IN, I, IL))THEN
            CALL PUSH(OUT, OL, F_CH)
            I=I+2
            GO TO 75
          END IF
          IF(STRMATCH('pi', IN, I, IL))THEN
            CALL PUSH(OUT, OL, C_PI)
            I=I+2
            GO TO 75
          END IF
          C=IN(I:I)
          IF(INDEX('+-*/^()', C) .NE. 0)THEN
            IF(C .EQ. '-' .AND.
     $         (     I .EQ. 1
     $          .OR. INDEX('(+-*/^',IN(I-1:I-1)) .NE. 0)
     $        )THEN
c             Unary minus
              CALL PUSH(OUT, OL, '!')
            ELSE
              CALL PUSH(OUT, OL, C)
            END IF
            I=I+1
            GO TO 75
          END IF
          CI=ICHAR(C)
          IF(      ICHAR('a') .LE. CI
     $       .AND. CI         .LE. ICHAR('z'))THEN
            CALL PUSH(OUT, OL, CHAR(CI+CAP))
            I=I+1
            GO TO 75
          END IF
          IF(      ICHAR('0') .LE. CI
     $       .AND. CI         .LE. ICHAR('9'))THEN
            CALL PUSH(OUT, OL, C)
            I=I+1
            GO TO 75
          END IF
          I=I+1
   75   IF(I .LE. IL)THEN
          GO TO 50
        END IF
        OL=OL-1
      END SUBROUTINE
      SUBROUTINE PUSH(OUT, OL, ATOM)
        CHARACTER OUT*(*), ATOM
        INTEGER OL
        OUT(OL:OL)=ATOM
        OL=OL+1
      END SUBROUTINE
      LOGICAL FUNCTION STRMATCH(S, IN, START, L)
        INTEGER I, START, L
        CHARACTER S*(*), IN*(*)
        IF(LEN(S) .GT. L-START)THEN
          STRMATCH=.FALSE.
          GO TO 200
        END IF
        STRMATCH=.TRUE.
        DO 100, I = 1,LEN(S)
          IF(S(I:I) .NE. IN(I+START-1:I+START-1))THEN
            STRMATCH=.FALSE.
            GO TO 200
          END IF
  100   END DO
  200   CONTINUE
      END FUNCTION
c vim: ts=2 sts=2 sw=2 tw=72 ft=fortran fenc=utf-8
